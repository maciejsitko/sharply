import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute} from 'react-router';
import CreateHistory from 'history/lib/createhashHistory';

//--> Routes imported go here -->
import Base from './routes/base';
import Home from './routes/home';
import Courses from './routes/courses';
//--> End of routes. -->

const History = new CreateHistory({
  queryKey: false
});

const Routes = (
  <Router history={History}>
    <Route path="/" component={Base} >
      <IndexRoute component={Home}/>
      <Route path="courses" component={Courses}/>
    </Route>
  </Router>
);

export default Routes;
