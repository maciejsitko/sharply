import { Dispatcher } from 'flux';

const flux = new Dispatcher();

export const register = (callback) => {
  return flux.register(callback);
};

export const dispatch = (actionType,action) => {
  return flux.dispatch(actionType,action);
};
