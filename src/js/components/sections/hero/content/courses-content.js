import React from 'react';
import Island from '../../../modules/island/island';

class CoursesContent extends React.Component {
  constructor() {
    super();
    this.state = {
      active: null,
      sHeight: 0,
      scroll: 0,
      clientHeight: 0
    };
  }
  componentDidMount() {
    this.parallax();
    this.setState({sHeight: document.body.scrollHeight});
  }
  parallax = () => {
    let clientHeight = document.querySelector('#course-content').clientHeight;
    this.setState({
      scroll: window.pageYOffset,
      clientHeight: clientHeight
    });
  }
  resetState() {
    console.log(this.state.active);
    this.setState({active: null});
  }
  componentWillMount() {
    window.addEventListener('scroll', this.parallax);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.parallax);
    console.log('unmounted');
  }
  selectCourse(type,e) {
    this.setState({active: type});
  }
  render() {
    let scrollHeight = this.state.scroll;
    let coords = {
      one : ((this.state.clientHeight * 0.3) + -scrollHeight) * 0.2 + 'px',
      two : ((this.state.clientHeight * 0.6) + -scrollHeight) * 0.5 + 'px',
      three : ((this.state.clientHeight * 0.3) + -scrollHeight) * 1.5 + 'px'
    };
    let computed = this.state.scroll > 0 ? (this.state.scroll / (this.state.sHeight - window.innerHeight)) * 100 : this.state.scroll;
    let cloud =  (-100 + Number(computed.toFixed(2))) + '%';
    return(
      <article id="course-content" style={{position: 'relative', height: window.innerHeight}}>
        <div className="fade-in-out" style={{position: 'absolute', left: 0, top: '50%', transform: 'translateY(-50%)', height: 700, width: '100%', backgroundImage: 'url(img/cloud.png)', backgroundSize: 'cover', backgroundPosition: 'center center'}}></div>
        <div  id="cloud-flying" style={{  position: 'absolute',
          left: cloud,
          top: coords.three,
          height: window.innerHeight/1.5,
          transform: 'translate(-2.5%, 150%)',
          width: '100%',
          zIndex: 50,
          backgroundImage: 'url(img/cloud-flying.png)',
          backgroundPosition: 'center left',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain'
        }}></div>
      <Island id="node" active={this.state.active} resetState={() => this.resetState() } onClick={this.selectCourse.bind(this, 'node')} img={'url(img/node.png)'} coords={{left:'65%', top: coords.one, height : window.innerHeight/4, width : window.innerHeight/4}}/>
      <Island id="js" active={this.state.active} resetState={() => this.resetState() } onClick={this.selectCourse.bind(this, 'js')} img={'url(img/js.png)'} coords={{left: '10%', top : coords.two, height : window.innerHeight/3, width : window.innerHeight/3}} />
      <Island id="react" active={this.state.active} resetState={() => this.resetState() } onClick={this.selectCourse.bind(this, 'react')} img={'url(img/react.png)'} coords={{left: '30%', top : coords.three,height : window.innerHeight/2,width : window.innerHeight/2}} />
      </article>
    );
  }
}



export default CoursesContent;
