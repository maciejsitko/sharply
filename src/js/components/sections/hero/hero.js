import React from 'react';
import Carousel from '../../modules/carousel/carousel';

const Hero = ({component, color}) => {
  let heroColor = color ? color : '';
    return(
        <section style={{overflow: 'hidden', height: window.innerHeight + 50, backgroundColor: heroColor}}>
          { React.createElement(component) }
        </section>

    );
};

export default Hero;
