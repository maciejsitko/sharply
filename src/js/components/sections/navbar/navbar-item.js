import React from 'react';
import { Link } from 'react-router';

const NavbarItem = ({tab, color, last}) => {
  let css = !last ? color : 'white'
  let goProClass = last ? 'go-pro' : '' ;
  return(
    <Link style={{color: css}} className={"nav-item " + goProClass} to={tab.path}>{tab.name}</Link>
  );
};

NavbarItem.defaultProps = {
    color: 'black'
}

export default NavbarItem;
