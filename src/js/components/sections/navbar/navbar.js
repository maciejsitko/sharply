import React from 'react';
import NavbarItem from './navbar-item';
import Image from '../../modules/image/image';
import Input from '../../modules/input/input';
import UIStore from '../../../stores/ui-store';
import ThemeStore from '../../../stores/theme-store';


class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      tabs: UIStore.getNavigation()
    };
    this.update = () => {
      // this.setState({
      //   theme: ThemeStore.getThemeColor()
      // });
    };
  }
  toggleNavbar = () => {
    this.setState({toggle: !this.state.toggle});
  }
  componentWillMount() {
    ThemeStore.addChangeListener(this.update);
  }
  componentWillUnmount() {
    ThemeStore.removeChangeListener(this.update);
  }
  render() {
    let theme = this.props.theme;
    let isActive = this.state.toggle === true;

    let navbarClass = isActive ? 'active' : '';
    let burgerClass = navbarClass;

    let stripeTheme = isActive ? theme.primary : theme.secondary;
    let lastTab = this.state.tabs.length;
    let navTabs = this.state.tabs.map((tab,index) => (
      <NavbarItem tab={tab} key={index} color={theme.primary} last={lastTab === (index+1)}/>
    ));
    return(
      <section>
        <nav>
          <section onClick={this.toggleNavbar} className={'burger-icon ' + burgerClass} >
            <div style={{background: stripeTheme}}></div>
            <div style={{background: stripeTheme}}></div>
            <div style={{background: stripeTheme}}></div>
          </section>
          <section style={{background: theme.secondary}} className={'nav-menu ' + navbarClass}>
            { navTabs }
          </section>
        </nav>
      </section>
    );
  }
}


export default Navbar;
