import React from 'react';

const FooterItem = ({tabs, theme}) => (
  <a style={{color: theme.primary}} href={tabs.path}>{tabs.name}</a>
);

export default FooterItem;
