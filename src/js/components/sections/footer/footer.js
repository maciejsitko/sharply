import React from 'react';
import FooterItem from './footer-item';
import SocialIcon from '../../modules/social/social-icon';
import store from '../../../mixins/store';
import ThemeStore from '../../../stores/theme-store';
import settings from '../../../constants/settings';

const Footer = ({tabs, social, theme}) => {
  let tabItems = tabs.map( (tab, index)  => <FooterItem tabs={tab} theme={theme} key={index} />);
  let socialItems = social.map( (item, index)  => <SocialIcon social={item} theme={theme} key={index} />);
  return(
    <footer style={{backgroundColor: theme.secondary, padding: '100px'}}>
      <section>
        { socialItems }
      </section>
      <section>
        { tabItems }
      </section>
    </footer>
  );
};

Footer.defaultProps = {
  tabs: settings.tabs,
  social: settings.social
};


export default Footer;
