import React from 'react';
import ThemeStore from '../../../stores/theme-store';
import Triangle from '../../modules/triangle/triangle';
import { connect } from '../../../mixins/store';
import { ArticleContent } from '../../modules/article/article';

const setTheme = () => {
  return {theme: ThemeStore.getThemeColor() };
};

const Main = ({theme, store}) => {
  let { getContent } = store;
  let random = Math.random();
  let img = `url(${theme.img}?a=${random})`;
  let fadeInOut = `fade-in-out-${theme.type}`;
  return(
    <article className={`main-content ${fadeInOut}`} style={{padding: '100px', backgroundColor: 'white', backgroundImage: img, height: window.innerHeight/1.5}}>
      <ArticleContent store={ store } getter={ getContent } color={theme.secondary} />
      <Triangle direction="down" color={theme.primary}/>
    </article>
  );
};

export default connect(Main, setTheme).store(ThemeStore);
