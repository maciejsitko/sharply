import React from 'react';


const Button = (props) => (
    <button className={props.className} onClick={props.btnAction}
            style={props.style}> {props.label} </button>
);

export default Button;
