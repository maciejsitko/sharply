import React from 'react';
import ContentStore from '../../../stores/course-store';
import Triangle from '../triangle/triangle';
import keydown from 'react-keydown';
import AppActions from '../../../actions/actions';

const scrollStyles = {
  position: 'relative',
  paddingBottom: '20px',
  cursor: 'pointer'
};

const fetchFeatured = () => {
    return {
      featured: ContentStore.getFeatured(),
      move: 0,
      moving: false,
      width: window.innerWidth,
      styles: scrollStyles
    };
};

class Carousel extends React.Component {
  constructor() {
    super();
    this.state = fetchFeatured();
    this.startPoint = 0;
  }
  adjustSize = () => {
    this.setState({width: window.innerWidth});
  }
  adjustBtnPos() {
    if(window.innerWidth < 900 && window.innerWidth > 600) {
      return (this.state.width/2) - (this.state.width * 0.50) /2;
    }
    if(window.innerWidth < 600) {
      return (this.state.width/2) - (this.state.width * 0.80) /2;
    }
    return (this.state.width/2) - (this.state.width * 0.20) /2;
  }
  randomSlide() {
    return Math.ceil((Math.random() * this.state.featured.length));
  }
  scrollTo(distance) {
    distance = distance === 0 ? this.state.width : distance;
    let start = Date.now();
    let currentScrollPos = this.state.move * this.state.width;
    let moveDirection = currentScrollPos >= distance ? 'left' : 'right';
    let move = this.state.move ;
    document.getElementById('hero-container').overflowX = 'scroll';
    var animFrame = (function moveAnimation() {
      let dist;
      let now = Math.abs(start - Date.now());
      switch(moveDirection) {
        case 'right':
          dist = currentScrollPos + (now / 750) * (distance / (move+1));
          break;
        case 'left':
          move = (move-1) > 0 ? move : (move+1);
          dist = currentScrollPos - (now / 750) * (distance / (move-1) );
          break;
      }
      document.getElementById('hero-container').scrollLeft = dist;
      if(now >= 750 ) {
        if(moveDirection === 'left' && distance === currentScrollPos) {
          document.getElementById('hero-container').scrollLeft = 0;
        } else {
          document.getElementById('hero-container').scrollLeft = distance;
        }
        window.cancelAnimationFrame(animFrame);
        document.getElementById('hero-container').overflowX = 'hidden';

        return;
      }
      window.requestAnimationFrame(moveAnimation);
    })(Date.now());
  }
  handleTouch(e) {
    this.startPoint = e.clientX;
  }
  handleTouchEnd(e) {
    e.preventDefault();
    let endPosition = e.clientX;
    let difference = this.startPoint - endPosition;
    if( Math.abs(difference) > 50) {
      this.setState({moving: true});
      if(this.startPoint > endPosition) {
        this.handleKeyPress({keyCode: 39});
      } else {
        this.handleKeyPress({keyCode: 37});
      }
    };
    this.setState({moving: false});
  }

  @keydown('left', 'right');
  handleKeyPress(e) {
    switch(e.keyCode) {
      case 37:
        if(this.state.move === 0) return;
        this.moveSlide('left', e);
      break;
      case 39:
        if(this.state.move === this.state.featured.length-1) return;
        this.moveSlide('right', e);
      break;
    }
  }
  moveSlide(direction,e) {
    let distance;
    let move;
    switch(direction) {
      case 'left':
        move = this.state.move - 1;
        distance = move * this.state.width;
        break;
      case 'right':
        move = this.state.move + 1;
        distance = move * this.state.width;
        break;
    }
    this.scrollTo(distance);
    this.setState({move: move });
  }
  componentWillMount() {
    ContentStore.addChangeListener(fetchFeatured);
    AppActions.changeTheme(this.state.featured[this.state.move].colors);
    window.addEventListener('resize', this.adjustSize);
  }
  componentWillUnmount() {
    ContentStore.removeChangeListener(fetchFeatured);
    window.removeEventListener('resize', this.adjustSize);
  }
  componentDidUpdate() {
    AppActions.changeTheme(this.state.featured[this.state.move].colors);
  }
  render() {
    let featured = this.state.featured;
    let styles = this.state.styles;
    let coords = {
      width: (featured.length * this.state.width),
      halfWidth: this.state.width/2,
      buttonLeft: this.state.width,
      btnCentered: this.adjustBtnPos()
    };
    let slides = featured.map(({src, colors, header, text, desc}, index) => {
      let hidden = {
          left: (index+1) === 1 ? 'hidden' : 'carousel-arrow',
          right: (index+1) === featured.length ? 'hidden' : 'carousel-arrow'
      };
        return(
            <div key={index} className="hero-field" style={{display: 'inline-block', minHeight: window.innerHeight, paddingTop: '100px', backgroundImage: `url(${src})`, backgroundColor: colors.primary, width: window.innerWidth, position: 'relative'}}>
              <header style={{color: colors.secondary, fontWeight: 'bold', position: 'absolute', left: 0, right: 0, margin: '0 auto', textAlign: 'center', top: '40%', transform: 'translateY(-40%)'}}>{ header }
                  <hr style={{borderColor: colors.secondary}}/>
                  <p>{text}</p>
              </header>
              <button style={{left: coords.btnCentered,  cursor: 'pointer',bottom: '30%', transform: 'translateY(30%)', background: colors.secondary}} className={`button`} >{ desc }</button>
              <Triangle onClick={this.moveSlide.bind(this,'right')} className={hidden.right} direction="right" color  ={colors.secondary} />
              <Triangle onClick={this.moveSlide.bind(this,'left')} className={hidden.left} direction="left" color={colors.secondary} />

            </div>
        );
    });
    return(
      <section onMouseDown={this.handleTouch.bind(this)} onMouseUp={this.handleTouchEnd.bind(this)}  id="hero-container" style={styles}>
        <div style={{width: coords.width, fontSize: '0'}}>{slides}</div>
      </section>
    );
  }
}

export default Carousel;
