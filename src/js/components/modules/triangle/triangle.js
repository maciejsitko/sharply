import React from 'react';

const Triangle = ({direction, color, size, className, onClick}) => {
  let styles = {
    height: 0,
    width: 0,
    cursor: 'pointer'
  };
  switch(direction) {
    case 'up':
      styles.borderLeft = `${size}px solid ${color}`;
      styles.borderTop = styles.borderBottom = `${size}px solid transparent` ;
      break;
    case 'left':
      styles.left = '5%';
      styles.borderRight =`${size}px solid ${color}`;
      styles.borderTop = styles.borderBottom = `${size}px solid transparent` ;
      break;
    case 'right':
      styles.right = '5%';
      styles.borderLeft = `${size}px solid ${color}`;
      styles.borderTop = styles.borderBottom = `${size}px solid transparent` ;
      break;
    case 'down':
      styles.borderTop = `${size}px solid ${color}`;
      styles.borderLeft = styles.borderRight = `${size}px solid transparent` ;
      break;
  }
  return (
    <div onClick={onClick}className={className} style={styles}></div>
  )
};

Triangle.defaultProps = {
  direction: 'right',
  color: '#333333',
  size: 10,
  className: 'triangle',
  onClick: () => {}
};

export default Triangle;
