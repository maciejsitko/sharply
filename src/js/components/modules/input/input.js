import React from 'react';

class Input extends React.Component {
  constructor() {
    super();
  }
  render() {
    return(
        <input type={this.props.type} />
    );
  }
}

export default Input;
