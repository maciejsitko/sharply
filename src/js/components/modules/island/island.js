import React from 'react';
import Button from '../button/button';
import { ArticleContent } from '../article/article';
import ContentStore from '../../../stores/content-store';

const Island = (props) =>  {
    let { getContent } = ContentStore;
    let { top, height, width} = props.coords;
    let mode =
    {
      active : props.active === props.id,
      disabled : props.active !== null && props.active !== props.id
    };
    let numericTop = Number(top.replace('px',''));
    let coords =
    {
        btn: {
          top: numericTop - (numericTop * 0.2)
        },
        island: {
          left: mode.active ? window.innerWidth/2 - Number(width)/2 : mode.disabled ? window.innerWidth : props.coords.left
        }
    };
    let btnStyle =
    {
      width: width,
      left: coords.island.left,
      top: coords.btn.top
    };
    return(
      <section>
        <Button btnAction={props.resetState} className="button-island" style={btnStyle} label="back" />
        <ArticleContent store={ ContentStore } getter={ getContent }/>
        <div onClick={props.onClick} className={`hover`} style={{left: coords.island.left,top,width,height,backgroundImage: props.img}}></div>
      </section>
    );
};


export default Island;
