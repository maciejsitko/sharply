import React from 'react';
import { Link } from 'react-router';

const SocialIcon = ({social, theme}) => (
  <div style={{display: 'inline-block', margin: '0 7.5px'}}>
    <a style={{color: theme.primary}} href={social.url}>
      <div style={{borderRadius: '100%', width: 50,textAlign: 'center', backgroundColor: theme.primary}}>
        <i style={{color: theme.secondary, fontSize: '25px',lineHeight: '50px', verticalAlign: 'middle'}} className={social.icon}></i>
      </div>
    </a>

  </div>
);

export default SocialIcon;
