import React from 'react';
import { connect } from '../../../mixins/store';


const getContent = ({getter}) => {
  return {content: getter() }
};

export const ArticleContent  = connect(({color, content}) => {
  let text = content[0];
  return(
    <section>
      <header style={{textAlign: 'center', color: color}}>{text.header}</header>
      <hr style={{borderColor: color}}/>
      <p style={{color: color, textAlign: 'justify'}}>{text.content}</p>
    </section>
  );
}, getContent).store();
