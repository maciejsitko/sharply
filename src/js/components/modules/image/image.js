import React from 'react';

const Image = ({src}) => (
  <img src={src} />
);


Image.defaultProps = {
  src: ""
};

export default Image;
