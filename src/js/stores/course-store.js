import AppConstants from '../constants/constants';
import { featured, get, set } from './api/featured';
import { create } from '../mixins/store';

const CourseStore = create(
    {
      getFeatured: () => {
        return featured;
      }
    }
);

export default CourseStore;
