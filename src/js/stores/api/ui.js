export const ui = {
    tabs: [
    {name: "Home", path: "/"},
    {name: "Courses", path: "/courses"},
    {name: "About", path: "/"},
    {name: "Support", path: "/"},
    {name: "Subscribe!", path: "/"}
  ],
  social: [
    {name: "Facebook",icon: "fa fa-facebook", url: "http://www.facebook.com"},
    {name: "Github", icon: "fa fa-github", url: "http://www.github.com"},
    {name: "Twitter", icon: "fa fa-twitter", url: "http://www.twitter.com"}
  ]
};

export const get = (element = null) => {
  return element ? ui[element] : ui;
};
