export const get = () => {
  var theme = JSON.parse(window.localStorage.getItem('theme'));
  theme = (typeof theme === 'undefined' || theme === null) ? featured[0].colors : theme;
  return theme;
};

export const set = (theme) => {
  return window.localStorage.setItem('theme', JSON.stringify(theme));
};

export const featured  = [
      {
        header: 'Thoroughly Learn Javascript.',
        src: 'http://www.w3devcampus.com/wp-content/uploads/logoAndOther/logo_JavaScript.png ',
        desc: 'Learn Javascript ',
        text: 'Learn how to use React and conquer User Interface market in no time. Hoow it up to one of the architectures of your preference, be it Flux or Redux.',
        colors: {
          primary: '#F0DB4F',
          secondary: '#323330',
          img: 'img/yellow-stop-two.gif',
          type: 'js'
        }
      },
      {
        header: 'Bootstrap your React Career.',
        src: 'http://2015.reactjsday.it/img/confs/reactjs/logo-white.svg',
        desc: 'Master React',
        text: 'Learn how to use React and conquer User Interface market in no time. Hoow it up to one of the architectures of your preference, be it Flux or Redux.',
        colors: {
          primary: '#00D8FF',
          secondary: '#007488',
          img: 'img/blue-stop.gif',
          type: 'react'
        }
      },
      {
      header: 'Node.js and Express.',
      src: 'http://alexeyza.com/images/nodejs.png',
      desc: 'Study Node',
      text: 'Learn how to use React and conquer User Interface market in no time. Hoow it up to one of the architectures of your preference, be it Flux or Redux.',
        colors: {
            secondary: '#88C043',
            primary: '#333333',
            img: 'img/brown-stop.gif',
            type: 'node'
        }
      }
      // ,
      // {
      // header: 'Sassy ways to styling. ',
      // src: 'http://sass-lang.com/assets/img/logos/logo-b6e1ef6e.svg',
      // desc: 'Sass It',
      // text: 'Learn how to use React and conquer User Interface market in no time. Hoow it up to one of the architectures of your preference, be it Flux or Redux.',
      //   colors: {
      //       primary: '#F9F9F9',
      //       secondary: '#CF649A'
      //   }
      // }
];
