import { featured } from './featured';
const defaultTheme = featured.map(f => f.colors)[0];

export default {
  current: defaultTheme,
  get()  {
    return this.current;
  },
  set(theme) {
    this.current = theme;
  }
};
