import AppConstants from '../constants/constants';
import { get, set, ui } from './api/ui';
import { create } from '../mixins/store';

const UIStore = create(
  {
    getNavigation() {
      return ui.tabs;
    },
    getWidgets() {

    }
  }
);


export default UIStore;
