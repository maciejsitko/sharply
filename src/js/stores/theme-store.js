import AppConstants from '../constants/constants';
import theme from './api/theme';
import { create } from '../mixins/store';

const ThemeStore = create(
    {
      getThemeColor: () => {
        return theme.get();
      }
    },
    (action) => {
      switch(action.actionType) {
        case AppConstants.THEME_CHANGE:
        theme.set(action.theme);
        break;
      }
      ThemeStore.emitChange();
    }
);

export default ThemeStore;
