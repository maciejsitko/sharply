import AppConstants from '../constants/constants';
import { get } from './api/content';
import { create } from '../mixins/store';

const ContentStore = create(
    {
      getContent: () => {
        return get();
      }
    }
);

export default ContentStore;
