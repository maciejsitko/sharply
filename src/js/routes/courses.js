import React from 'react';
import Hero from '../components/sections/hero/hero';
import CoursesContent from '../components/sections/hero/content/courses-content';

class Courses extends React.Component {
  constructor() {
    super();
  }
  render() {
    return(
      <section>
        <Hero color="#6AE4FA" component={ CoursesContent }/>
      </section>
    );
  }
}

export default Courses;
