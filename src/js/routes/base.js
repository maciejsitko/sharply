import React from 'react';
import Navbar from '../components/sections/navbar/navbar';
import Footer from '../components/sections/footer/footer';
import { connect } from '../mixins/store';
import ThemeStore from '../stores/theme-store';

const setTheme = () => {
  return {theme: ThemeStore.getThemeColor() };
};

const Base = (props) => (
    <main>
      <Navbar theme={props.theme} />
        <section>
          {props.children}
        </section>
      <Footer theme={props.theme} />
    </main>
);

export default connect(Base, setTheme).store(ThemeStore);
