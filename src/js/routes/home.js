import React from 'react';
import Hero from '../components/sections/hero/hero';
import Main from '../components/sections/main/main';
import Carousel from '../components/modules/carousel/carousel';

import ContentStore from '../stores/content-store';


const Home = (props) => {
    return(
      <section>
        <Hero component={ Carousel }/>
        <Main store={ ContentStore }/>
      </section>
    );
};

export default Home;
