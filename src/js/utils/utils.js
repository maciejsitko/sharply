export const type = (o) => {
  let _type = {}.toString;
  return _type.call(o).split(" ")[1].replace(']','').toLowerCase();
}

export const comp = (...fns) => arg => fns.reverse().reduce((result,fn) => fn(result), arg);

export const partial = (fn, ...args) => {
    const arity = fn.length;
    const getArgs = (totalArgs) => {
        return (...zargs) => {
            let nextTotalArgs = [...totalArgs, ...zargs];
            if (nextTotalArgs.length >= arity)
                return fn(...nextTotalArgs);
            else
                return getArgs(nextTotalArgs);
        }
    }

    if(args.length === arity) return fn(...args);

    return getArgs(args);
}

export const rand = (min = 0) => {
  return {
    to: (max, floor = false) => {
      let func = floor ? Math.floor : res => res;
      return func(Math.random() * (max-min) + min);
    }
  }
}

export const range = (a = 0) => {
  return {
    to: (b) => {
        let range = [];
        for(let i = a; i <= b; i++) {
          range.push(i);
        }
        return range;
     }
  }
}
