import AppConstants from '../constants/constants';
import { dispatch, register } from '../dispatchers/dispatcher';

export default {
  getFeatured() {
    dispatch({

    });
  },
  changeTheme(theme) {
    dispatch({
      actionType: AppConstants.THEME_CHANGE, theme
    });
  }
};
