import { EventEmitter } from 'events';
import { dispatch, register} from '../dispatchers/dispatcher';
import React from 'react';
const CHANGE_EVENT = 'change';

export const create = (logic, registrar = () => {}) => {
    return Object.assign(EventEmitter.prototype,{
      emitChange() {
        this.emit(CHANGE_EVENT);
      },
      addChangeListener(callback) {
        this.on(CHANGE_EVENT,callback);
      },
      removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT,callback);
      },
      ...logic,
      dispatcherIndex: register(registrar)
    });
  };

export const connect = (ReactComponent, callback) => {
      return  {
        store: (store) => {
          return class extends React.Component {
            constructor(props) {
              super(props);
              store = store || props.store;
              this.state = callback(props);
              this._onChange = this._onChange.bind(this);
            }
            componentWillMount() {
              store.addChangeListener(this._onChange);
            }
            componentWillUnmount() {
              store.removeChangeListener(this._onChange);
            }
            _onChange() {
              this.setState(callback(this.props));
            }
            render() {
              return <ReactComponent {...this.state} {...this.props} />;
            }
        }
      }
    };
};
