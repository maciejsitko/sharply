import React from 'react';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
expect.extend(expectJSX);

/* Get Component Here --> */
import { Carousel } from '../src/components/carousel/carousel';
/* End --> */

describe('TestCarousel', () => {
  it('should have the same JSX output',()=> {
    const renderer = TestUtils.createRenderer();
    renderer.render(<Carousel greeting='hello' />);
    const actual = renderer.getRenderOutput();
    const expected = (
      <div>hello</div>
    );
    expect(actual).toIncludeJSX(expected);
  });
});
